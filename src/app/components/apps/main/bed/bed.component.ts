import { Component, OnInit } from '@angular/core';
import { Table } from 'primeng/table';
import { BedService } from './bed-service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-setup-bed',
    templateUrl: './bed.component.html',
    styleUrls: ['./bed.component.scss'],
})
export class BedComponent {
    blockedPanel: boolean = false;

    data: any[] = [];
    type: any[] = [];
    ward: any[] = [];
    selectecId: any;
    selectedData: any;
    
    isNew: boolean = false;
    displayDialog: boolean = false;

    data_id: any;
    data_name: any;
    data_description: any;
    data_status: boolean = true;

    messages : any = '';

    title = 'เตียง';

    constructor(
        private bedService: BedService,
        private router: Router
    ) {}

   async ngOnInit() {
        await this.getBedType();
        await this.getData();
    }

    async getData() {
        try{
            const res: any = await this.bedService.get();
        if(res.data.data){
            this.data=res.data.data;
            for(let i of this.data){
                i.bed_type = this.type.find((x:any)=> i.bed_type_id == x.id).name
            }


        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
    }

    async getBedType() {
        try{
            const res: any = await this.bedService.getBedType();
        if(res.data.data){
            this.type=res.data.data;
        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
    }

    async getWard() {
        try{
            const res: any = await this.bedService.getWard();
        if(res.data.data){
            this.type=res.data.data;
        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
    }

    async saveData() {
        let info={
            description: this.data_name,
            is_active: this.data_status
        }

        try{
            let res: any=await this.bedService.save(info)
        }catch(error){
            console.log(error)
        }
        this.clearData();
        this.displayDialog = false;
        this.getData();
    }

    async updateData() {
        let info={
            description: this.data_name,
            is_active: this.data_status
        }

        try{
            let res: any=await this.bedService.update(this.data_id,info)
        }catch(error){
            console.log(error)
        }
        this.clearData();
        this.displayDialog = false;
        this.getData();
    }

    async deleteData(data: any) {
        console.log(data);
        let id=data.id;
        try{
            let res: any=await this.bedService.delete(id)
        }catch(error){
            console.log(error)
        }
        this.clearData();
        this.displayDialog = false;
        this.getData();
    }

    async showEditDialog(data: any) {
        this.isNew = false;
        this.displayDialog = true;
        this.data_name=data.description;
        this.data_id=data.id;
        this.data_status=data.is_active;

    }

    async showAddDialog() {
        this.isNew = true;
        this.displayDialog = true;

    }

    async showDeleteDialog() {
    }

    clearData() {
        this.data_id = '';
        this.data_name = '';
        this.data_description = '';
        this.data_status = true;
    }

    back() {
        this.router.navigate(['/']);
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }

}