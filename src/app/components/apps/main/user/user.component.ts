import { Component, OnInit } from '@angular/core';
import { Table } from 'primeng/table';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
})

export class UserComponent {
    blockedPanel: boolean = false;
    user_id: any;
    cid: any;
    fullname: any;
    phone_number: any;
    position: any;
    title: any;
    is_hospital: boolean = true;
    is_service: boolean = false;
    hospitals: any;

    cidEdit: any;
    fullNameEdit: any;
    hospitalId: any;
    phoneNumberEdit: any;
    posiTionEdit: any;
    tiTleEdit: any;

    selectService: any;
    services: any;

    serviceall: any;
    serviceId: any;
    hospitalBack: any;

    // edit Roles
    selectedDrop: any;
    // roles
    roles: any;
    profiles: any;
    rolesall: any;

    users: any;
    displayForm: boolean = false;

    userName: string = '';
    passWord: string = '';
    usersActive: boolean = true;

    userIdEdit: number = 0;
    userNameEdit: string = '';
    passWordEdit: string = '';
    passWordD: string = '';
    userActiveEdit: boolean = true;

    isAdd: boolean = false;
    isEdit: boolean = false;

    messages: any | undefined;

    usersStatus = [
        { label: 'Active', value: true },
        { label: 'Inactive', value: false },
    ];

    isValidate: boolean = true;
    constructor(

    ) {}

    async ngOnInit() {

    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }

    // get profiles
    async getProfilesData(id: any) {

    }

    // get Roles
    async getRolesData(id: any) {

    }

    // get RolesAll
    async getRolesAllData() {

    }

    // get Service จุดบริการ
    async getServiceData() {

    }

    // get Services data from API แสดงหน้าแรก
    async getData() {

    }


    // display form add data
    displayFormAdd() {
        this.displayClear();
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;

    }
    
    displayClear() {

    }

    // display form edit data
    async displayFormEdit(data: any) {
        this.displayClear();

        this.isAdd = false;
        this.isEdit = true;
        // edit profile

    }

    async save() {
        this.saveUser();
    }

    async saveProfiles() {

    }

    // function save data
    async saveUser() {
        this.isValidate = true;

        // Validate ชื่อ-สกุล


        // Validate เลขบัตรประชาชน


         // Validate ตำแหน่ง


        // Validate เบอร์โทรศัพท์


        // Validate รหัสผ่าน


        if (this.isValidate) {

        }
    }

    async update() {
        this.updateUser();
    }

    showDropdownService() {
        this.is_service = true;

    }

    // function update data profile
    async updateProfile() {
        let id = this.userIdEdit;

        this.isValidate = true;
     
    }

    async updateUser() {

 
        // save data

        // close form
        this.displayForm = false;

        //refresh data

    }

    // set is active = false
    async disable(id: number) {

    }

    // set is active = true
    async enable(id: number) {

    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    async showDropdown(id: any) {

    }

    // alert message
    showMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },
        ];
    }

    clearMessages() {
        this.messages = [];
    }

    isNumber(value?: string | number) {
        return (
            value != null && value !== '' && !isNaN(Number(value.toString()))
        );
    }
}
