import { Component, OnInit } from '@angular/core';
import { Table } from 'primeng/table';
import { StandingOrderService } from './standing-order-service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-setup-standing-order',
    templateUrl: './standing-order.component.html',
    styleUrls: ['./standing-order.component.scss'],
})
export class StandingOrderComponent {
    blockedPanel: boolean = false;

    data: any[] = [];
    selectecId: any;
    selectedData: any;
    
    isNew: boolean = false;
    displayDialog: boolean = false;

    data_code: any;
    data_name: any;
    data_description: any;
    data_status: boolean = true;

    messages : any = '';

    title = '   การประเมินพยาบาล';

    constructor(
        private standingOrderService: StandingOrderService,
        private router: Router
    ) {}

    ngOnInit() {

    }

    async getData() {
    }

    async saveData() {

        this.clearData();
        this.displayDialog = false;
    }

    async updateData() {

        this.clearData();
        this.displayDialog = false;
    }

    async deleteData() {
    }

    async showEditDialog(data: any) {
        this.isNew = false;
        this.displayDialog = true;
    }

    async showAddDialog() {
        this.isNew = true;
        this.displayDialog = true;

    }

    async showDeleteDialog() {
    }

    clearData() {
        this.data_code = '';
        this.data_name = '';
        this.data_description = '';
        this.data_status = true;
    }

    back() {
        this.router.navigate(['/']);
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }

}